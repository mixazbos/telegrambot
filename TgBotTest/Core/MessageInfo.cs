﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;

namespace TgBotTest.Core
{
    class MessageInfo
    {
        /// <summary>
        /// Text a received message
        /// </summary>
        public string MessageText { get; set; }

        /// <summary>
        /// Message char ID
        /// </summary>
        public long ChatId { get; set; }

        /// <summary>
        /// Message sender
        /// </summary>
        public User Sender { get; set; }

        public MessageInfo(Message msg)
        {
            GetMessageInfo(msg);
        }

        /// <summary>
        /// Get message info
        /// </summary>
        /// <param name="msg">Received message</param>
        private void GetMessageInfo(Message msg)
        {
            if (msg == null) return;
            MessageText = msg.Text;
            ChatId = msg.Chat.Id;
            Sender = msg.From;
        }
    }
}
