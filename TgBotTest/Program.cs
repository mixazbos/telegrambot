﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TgBotTest.Core;

namespace TgBotTest
{
    internal class Program
    {
        private static TelegramBotClient TelegramBot;
        private static CancellationTokenSource cts = new CancellationTokenSource();

        private static string _mode = "def";
        private static User _admin = null;

        private static ReceiverOptions receiverOptions = new ReceiverOptions
        {
            AllowedUpdates = Array.Empty<UpdateType>()
        };
    static void Main(string[] args)
        {
            TelegramBot = new TelegramBotClient(Constants.Token);
            TelegramBot.StartReceiving(
                updateHandler: HandleUpdateAsync,
                pollingErrorHandler: HandlePollingErrorAsync,
                receiverOptions: receiverOptions,
                cancellationToken: cts.Token
                );
            Console.ReadLine();
        }

    static void Init()
    {
        List<Command> commands = new List<Command>()
        {
            new Command(){CommandText = "me", ResponseText = ""},
            new Command(){CommandText = "setadmin", ResponseText = ""},
            new Command(){CommandText = "getadmin", ResponseText = ""},
            new Command(){CommandText = "bot", ResponseText = ""},
            new Command(){CommandText = "edit", ResponseText = ""}
        };
        
    }

    /// <summary>
    /// Chat updates handler
    /// </summary>
    /// <param name="botClient">Telegram update handler</param>
    /// <param name="update">Update type</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns></returns>
        static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update == null) return;
            var message = update.Message;
            var msgInfo = new MessageInfo(message);

            //var a = GetAdminLocale(user);
            
            if (_mode == "setadmin" && msgInfo.MessageText == "/me")
            {
                _admin = msgInfo.Sender;
                await SendMsg("✅ Администратор бота установлен", msgInfo.ChatId, botClient, cancellationToken);
                _mode = "def";
            }
            /*System.Timers.Timer timer = new System.Timers.Timer(3000);
            timer.Elapsed += (sender, e) => {
                SendMsg($"сообщение {e.SignalTime.Ticks}", chatId, botClient, cancellationToken);
            };
            timer.Start();*/

            var commandAction = GetTgCommands(msgInfo.MessageText, msgInfo.Sender);//, timer);

            if (!string.IsNullOrEmpty(commandAction))
            {
                await SendMsg(commandAction, msgInfo.ChatId, botClient, cancellationToken);
            } else
            {
                await SendMsg("Your message:\n" + msgInfo.MessageText, msgInfo.ChatId, botClient, cancellationToken);
            }
        }

        private static async Task SendMsg(string messageText, long chatId, ITelegramBotClient botClient, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Received a '{messageText}' message in chat {chatId}.");

            Message sentMessage = await botClient.SendTextMessageAsync(
                chatId: chatId,
                text: messageText,
                cancellationToken: cancellationToken);
        }

        private static string GetTgCommands(string command, User user)//, System.Timers.Timer timer)
        {
            //timer.Stop();
            var cmd = command.Remove(0, 1);
            switch (cmd)
            {
                case "bot":
                    return "обращение к боту";
                case "edit":
                    if (_mode == "def")
                    {
                        return "редактирование бота";
                    }
                    else return GetUserMode();
                case "setadmin":
                    if (_mode != "setadmin" && (_admin == null || _admin.Id == null))
                    {
                        _mode = "setadmin";
                        return "✅ Режим добавления администратора\n/me - указать себя";
                    } else
                    {
                        _mode = "def";
                        return "❌ Назначить администратора можно только один раз";
                    }
                case "me":
                    return GetUserInfo(user);
                case "getadmin":
                    return "Администратор\n"+GetUserInfo(_admin);

            }
            return "❌ Такой команды нет";
        }

        static string SetBotLang(string lng)
        {
            if (lng == null) return "Нет такого языка";
            
            return "";
        }

        static string GetUserMode()
        {
            // TODO проверить пермишены пользователя и тд
            return "Бот сейчас в  режиме редактирования";
        }

        static string GetUserInfo(User user)
        {
            if (user == null) return "Администратор бота не назначен";
            return $"😉 Пользователь {user.FirstName ?? user.LastName}\nИмя пользователя - {user.Username ?? "Нет" }\nTG ID is {user.Id}";
        }

        static Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception.Message;

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
    }
}
